# Mapping NBA travel
This is an article on visualising / animating travel data

I use data of 19-20 NBA season travel data.

## Data
- srcdata/2020_nba_schedule.csv collects the schedule data
- srcdata/arenas_list.csv is the list of areans & locations
- srcdata/teamcolor_dict.pickle is a dictionary of team colours 

### Article
Link: TBC