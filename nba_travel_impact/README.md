# Understanding the impact of travel, home/away and rest in the NBA
Processing and visualizing multiple categorical variables with Python — NBA’s schedule challenges 

Using 2018-2019 NBA season data. 

## Data
- srcdata/2020_nba_schedule.csv collects the schedule data
- srcdata/2019_nba_team_box_scores.csv and 2019_nba_team_box_scores_2.csv collect team box scores with some additional data. 

### Article
Link: TBC